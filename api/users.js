
// Setting up a middleware to use for all requests
var express = require('express');
var router = express.Router();
var User = require('../models/user');
router.use(function(req, res, next){
    // Some logging goes here
    console.log('Something is happening');
    next(); // Make sure that we go to the next route and don't stop here!
});


// API route =============================

router.route('/users')
    .post(function(req, res){
        var user = new User();
        console.log('body: ',req.body.name);
        user.name = req.body.name;

        user.save(function(err){
            if(err) res.send(err);
            res.json({message : "User Created!"});
        });
    })
    .get(function(req, res){
        User.find(function(err, users){
            if(err) {
                res.send(err)
            } else if ( !users.length ){
                res.json({message : 'Sorry there is no users in our database!'});
            };
            res.json(users);
        });
    });

router.route('/users/:user_id')
    .get(function(req, res){
        User.findById( req.params.user_id, function(err, user){
            if (err) res.send(err);
            res.json(user);
        });
    })
    .put(function(req,res){
        User.findById(req.params.user_id, function(err,user){
            if(err) res.send(err);

            user.name = req.body.name;
            user.save(function(err){
                if (err) res.send(err);
                res.json({message: 'User name has been modifyed successfully!'});
            });
        });
    })
    .delete(function(req,res){
        User.findById(req.params.user_id, function(err, user){
            if (err) res.send(err);
            res.userName = user.name;
        });
        User.remove({ _id: req.params.user_id}, function(err, user){
            if (err) res.send(err);

            res.json({message : res.userName + ' Deleted!'});


        });
    });

module.exports = router;