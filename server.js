// SERVER
var express = require('express');
var http = require('http');
var app = require('./app');

// Server Config
var server = http.createServer(app);
var options = {
    port : {
        dev: 3000,
        prod: 6000
    }
};

server.listen(options.port.dev, function(err) {
    if (err) {
        console.log(err)
    } else {
        console.log( 'Magic Happens on http://localhost:' + options.port.dev + '/');
    }
});

